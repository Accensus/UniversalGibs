Model UG_Gib_Obj_HL0
{
	Path "Models/Half-Life"
	Scale 0.8 0.8 0.8
	USEACTORPITCH
	INHERITACTORROLL

	Model 0 "HGIB_B_Bone1.md3"
	SurfaceSkin 0 0 "B_Bone1.png"
	FrameIndex HLG0 A 0 0
}

Model UG_Gib_Obj_HL1
{
	Path "Models/Half-Life"
	Scale 0.8 0.8 0.8
	USEACTORPITCH
	INHERITACTORROLL

	Model 0 "HGIB_B_Gib1.md3"
	SurfaceSkin 0 0 "B_Gib1.png"
	FrameIndex HLG1 A 0 0
}

Model UG_Gib_Obj_HL2
{
	Path "Models/Half-Life"
	Scale 0.8 0.8 0.8
	USEACTORPITCH
	INHERITACTORROLL

	Model 0 "HGIB_Guts1.md3"
	SurfaceSkin 0 0 "GUTS1_Top1.png"
	FrameIndex HLG2 A 0 0
}

Model UG_Gib_Obj_HL3
{
	Path "Models/Half-Life"
	Scale 0.8 0.8 0.8
	USEACTORPITCH
	INHERITACTORROLL

	Model 0 "HGIB_HMeat1.md3"
	SurfaceSkin 0 0 "HMEAT1_Front1.png"
	FrameIndex HLG3 A 0 0
}

Model UG_Gib_Obj_HL4
{
	Path "Models/Half-Life"
	Scale 0.8 0.8 0.8
	USEACTORPITCH
	INHERITACTORROLL

	Model 0 "HGIB_Legbone1.md3"
	SurfaceSkin 0 0 "G_legbone1.png"
	FrameIndex HLG4 A 0 0
}

Model UG_Gib_Obj_HL5
{
	Path "Models/Half-Life"
	Scale 0.8 0.8 0.8
	USEACTORPITCH
	INHERITACTORROLL

	Model 0 "HGIB_Lung1.md3"
	SurfaceSkin 0 0 "Lung1.png"
	SurfaceSkin 0 1 "Rib1.png"
	FrameIndex HLG5 A 0 0
}

Model UG_Gib_Obj_HL6
{
	Path "Models/Half-Life"
	Scale 0.8 0.8 0.8
	USEACTORPITCH
	INHERITACTORROLL

	Model 0 "HGIB_Skull1.md3"
	SurfaceSkin 0 0 "Skull1.png"
	SurfaceSkin 0 1 "Side_Bone1.png"
	SurfaceSkin 0 2 "Skin_Side1.png"
	SurfaceSkin 0 3 "Neck_1.png"
	FrameIndex HLG6 A 0 0
}

Model UG_Gib_Obj_HL7
{
	Path "Models/Half-Life"
	Scale 0.8 0.8 0.8
	USEACTORPITCH
	INHERITACTORROLL

	Model 0 "Flesh1.md3"
	SurfaceSkin 0 0 "Flesh1.png"
	SurfaceSkin 0 1 "Flesh1Top.png"
	Offset 0 0 1.5
	FrameIndex HLG7 A 0 0
}

Model UG_Gib_Obj_HL8
{
	Path "Models/Half-Life"
	Scale 0.8 0.8 0.8
	USEACTORPITCH
	INHERITACTORROLL

	Model 0 "Flesh2.md3"
	SurfaceSkin 0 0 "Flesh2.png"
	Offset 0 0 1.5
	FrameIndex HLG8 A 0 0
}

Model UG_Gib_Obj_HL9
{
	Path "Models/Half-Life"
	Scale 0.8 0.8 0.8
	USEACTORPITCH
	INHERITACTORROLL

	Model 0 "Flesh3.md3"
	SurfaceSkin 0 0 "Flesh2.png"
	FrameIndex HLG9 A 0 0
}

Model UG_Gib_Obj_HL10
{
	Path "Models/Half-Life"
	Scale 0.8 0.8 0.8
	USEACTORPITCH
	INHERITACTORROLL

	Model 0 "Flesh4.md3"
	SurfaceSkin 0 0 "Flesh2.png"
	Offset 0 0 1.5
	FrameIndex HLGA A 0 0
}